CPP_FILES := $(shell find -name "*.cpp")
OBJ_FILES := $(CPP_FILES:.cpp=.o)

$(_CPP_AUTORUN_TARGET): $(OBJ_FILES)
	g++ $(OBJ_FILES) -o $@

# Watchmedo throws error if target doesn't exist.
run:
	./$(_CPP_AUTORUN_TARGET)

%.o: %.cpp
	g++ -Wall -c $< -o $@

.PHONY: clean
clean:
	rm -rf *.o
	rm $(_CPP_AUTORUN_TARGET) 

auto-compile:
	watchmedo auto-restart -p "*.cpp" -d $(_CPP_AUTORUN_WATCH) make