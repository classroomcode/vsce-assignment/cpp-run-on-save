import os
import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--target", type=str, default="a.out")
parser.add_argument("--directory", type=str, default=os.path.abspath(os.getcwd()))
args = parser.parse_args()

os.environ["_CPP_AUTORUN_TARGET"] = args.target
os.environ["_CPP_AUTORUN_WATCH"] = args.directory

subprocess.Popen(["make", "auto-compile"])
subprocess.call(f"watchmedo auto-restart -p '{args.target}' make run", shell=True)